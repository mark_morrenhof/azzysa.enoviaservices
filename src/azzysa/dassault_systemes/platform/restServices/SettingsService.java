package azzysa.dassault_systemes.platform.restServices;

import com.dassault_systemes.platform.restServices.RestService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/settings")
public class SettingsService extends RestService {
	
	@GET
	@Path("/get/{site}/{server}")
	@Produces (MediaType.APPLICATION_JSON)
	public Response getSettings(@Context HttpServletRequest req, @PathParam("site") String siteName, @PathParam("server") String machineName){
		try {	
			// Get context
			matrix.db.Context context = authenticate(req);
			context.connect();
			
			// Get settings
			return Response.status(200).entity(infostrait.azzysa.ev6.settings.Provider.getInstance().Initialize(context, siteName, machineName)).build();
		}
		catch (Exception ex) { 
			return Response.status(500).entity(ex).build(); 
		} 		
	}
}