package azzysa.dassault_systemes.platform.restServices;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import com.matrixone.servlet.Framework;

import matrix.util.MatrixException;

import com.dassault_systemes.platform.restServices.RestService;

@Path("/login")
public class LoginService extends RestService {
    
	/**
	 * Check whether the Matrix Session (matrix.db.Context) contains a valid session and context user.
	 * 
	 * @param context
	 * @return name of user when session exists.
	 */
	@GET
	@Path("/validate")
	@Produces (MediaType.APPLICATION_JSON)
	public Response ValidateSession(@Context HttpServletRequest req) {	
		try {			
			// Get context
			matrix.db.Context context = authenticate(req);
			context.connect();
			
			// Check context
			return Response.status(200).entity((context.isConnected() && context.isContextSet() && context.checkContext())).build();
		} catch (Exception ex) {
			// Invalid context, do not throw error but return false for invalid context
			return Response.status(200).entity(false).build(); 
		}
	}
	
	@SuppressWarnings({ "unused", "deprecation" })
	@GET
	@Path("/authenticate/{username}/{password}/{vault}")
	@Produces (MediaType.APPLICATION_JSON)
	public Response authenticateUser(@Context HttpServletRequest req, @PathParam("username") String userName, @PathParam("password") String password, @PathParam("vault") String vault){
		try {
			matrix.db.Context context = null;
	        if(Framework.isLoggedIn(req)) {
	            context = Framework.getContext(req.getSession(false));
	            
	            // Check user name of the connected context with the user name parameter
	            if (!context.getUser().equalsIgnoreCase(userName)) {
	            	// Different user name -> reconnect the current context
	                context = new matrix.db.Context("");
	                context.setUser(userName);
	                context.setPassword(password);
	                context.setVault(vault);
	                try {
		                context.connect();
	                } catch (MatrixException ex) {
	        			return Response.ok(false).build();
	                }
	            }
	        } else {
		        if(context == null)
		        {
	                context = new matrix.db.Context("");
	                context.setUser(userName);
	                context.setPassword(password);
	                context.setVault(vault);
	                try {
		                context.connect();
	                } catch (MatrixException ex) {
	        			return Response.ok(false).build();
	                }
		        } else {	        
					context.resetContext(userName, password, vault);
	                try {
		                context.connect();
	                } catch (MatrixException ex) {
	        			return Response.ok(false).build();
	                }
		        }		

				context.setSessionId(req.getSession().getId());
				context.instantiateContext();
				
				com.matrixone.servlet.ServletUtil.putSessionValue(req.getSession(),
						"ematrix.context", context);
				com.matrixone.servlet.ServletUtil.putSessionValue(req.getSession(),
						"ematrix.bindings.listener",
						new com.matrixone.servlet.MatrixBindingListener(context));
	        }
			
			// Return result
			return Response.ok(true).cookie(new NewCookie("JSESSIONID", req.getSession().getId())).build();
		}
		catch (Exception ex) { 
			return Response.status(500).entity(ex).build(); 
		} 		
	}
}