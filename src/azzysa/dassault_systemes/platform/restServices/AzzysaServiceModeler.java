package azzysa.dassault_systemes.platform.restServices;

import javax.ws.rs.ApplicationPath;
import com.dassault_systemes.platform.restServices.ModelerBase;

@ApplicationPath(ModelerBase.REST_BASE_PATH + "/AzzysaServiceModeler")
public class AzzysaServiceModeler extends ModelerBase {

	@Override
	public Class<?>[] getServices() {
		return new Class<?>[] {SettingsService.class, FrameworkService.class, LoginService.class};
	}

}