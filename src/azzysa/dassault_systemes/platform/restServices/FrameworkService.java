package azzysa.dassault_systemes.platform.restServices;

import com.dassault_systemes.platform.restServices.RestService;
import infostrait.azzysa.ev6.commonobjects.DataObject;
import infostrait.azzysa.ev6.commonobjects.DataPackage;
import infostrait.azzysa.ev6.commonobjects.Mapping;
import infostrait.azzysa.ev6.commonobjects.ProcessResult;
import infostrait.azzysa.ev6.commonobjects.Utility;
import infostrait.azzysa.ev6.commonobjects.eLogSeverity;
import vanderlande.http.CompressibleRestService;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/framework")
public class FrameworkService extends CompressibleRestService {

	@POST
	@Consumes (MediaType.APPLICATION_JSON)
	@Path("/process")
	@Produces (MediaType.APPLICATION_JSON)
	public Response getSettings(@Context HttpServletRequest req, DataPackage inputPackage) {
        DataPackage _ReturnPackage;
        ClassLoader _ClsLdr;
        Mapping mapping;
        ProcessResult invocationResult;
        				
		try {
			// Check the package parameter
            if (inputPackage == null) {
                // Unable to determine mapping, not expected
                Utility.Log("Null input package", eLogSeverity.error);
                throw new Exception("Null input package");
            }
            if (inputPackage.MappingReference == null) {
                // Unable to determine mapping, not expected
                Utility.Log("Unable to determine mapping, not expected", eLogSeverity.error);
                throw new Exception("Unable to determine mapping");
            }
            
			// Connect to Matrix
			matrix.db.Context context = getContext(req);
			context.connect();
			
			// Turn of history for Sync / Import
			vanderlande.common.MQLUtils.execute(context,  "history $1", Arrays.asList("off"));
						
			// Process package
	        // Create log writer
	        _ReturnPackage = null;
	                    
            // Create return package (contains all object that failed during the import)
            _ReturnPackage = new DataPackage();
            _ReturnPackage.PackageId = inputPackage.PackageId;
            _ReturnPackage.MappingReference = inputPackage.MappingReference;
            if (inputPackage.DataObjects == null || inputPackage.DataObjects.ObjectCollection == null || inputPackage.DataObjects.ObjectCollection.DataObject == null) {    	        
    	        // No objects to process
    			return Response.status(200).entity(_ReturnPackage).build();
            }
            if (inputPackage.DataObjects.ObjectCollection.DataObject.isEmpty()) {    	        
    	        // No objects to process
    			return Response.status(200).entity(_ReturnPackage).build();
            }

            // Construct class loader to use for processing this package
            _ClsLdr = getClass().getClassLoader();
            
            // Get data model mapping from ENOVIA by reference from the package
            Utility.Log("Get data model mapping from ENOVIA by reference from the package", eLogSeverity.information);
            mapping = Utility.getMapping(context, inputPackage.MappingReference);
                    
            // Sort objects accordingly the sort expression
            Utility.Log("Sort objects accordingly the sort expression", eLogSeverity.information);
            List<DataObject> dataObjects;
            if (mapping.SortAttribute != null) {
                if (mapping.SortAttribute.toLowerCase().startsWith("smarteamrevisionsort")) {
                    // SmarTeam revision sort
                    Utility.Log("SmarTeam revision sort", eLogSeverity.information);
                    
                    String originAttributeName = mapping.SortAttribute.substring(mapping.SortAttribute.indexOf("[") + 1, mapping.SortAttribute.indexOf(";"));
                    String revisionAttributeName = mapping.SortAttribute.substring(mapping.SortAttribute.indexOf(";") + 1, mapping.SortAttribute.lastIndexOf(";"));
                    String parentRevisionAttributeName = mapping.SortAttribute.substring(mapping.SortAttribute.lastIndexOf(";") + 1, mapping.SortAttribute.indexOf("]"));
            
                    dataObjects = inputPackage.DataObjects.ObjectCollection.GetObjectsSmarTeamRevisionSorted(originAttributeName, revisionAttributeName, parentRevisionAttributeName);
                } else if (mapping.SortAttribute.toLowerCase().startsWith("smarteamcreationdatesort")) {
                    // SmarTeam revision/creation date sort
                    Utility.Log("SmarTeam revision/creation date sort", eLogSeverity.information);
                    
                    String originAttributeName = mapping.SortAttribute.substring(mapping.SortAttribute.indexOf("[") + 1, mapping.SortAttribute.indexOf(";"));
                    String creationDateAttributeName = mapping.SortAttribute.substring(mapping.SortAttribute.lastIndexOf(";") + 1, mapping.SortAttribute.indexOf("]"));
            
                    dataObjects = inputPackage.DataObjects.ObjectCollection.GetObjectsSortedByDate(originAttributeName, creationDateAttributeName);
                } else {
                    // Attribute sort
                    Utility.Log("Attribute sort", eLogSeverity.information);
                    dataObjects = inputPackage.DataObjects.ObjectCollection.GetObjectsSorted(mapping.SortAttribute);
                }
            } else {
                // Do not sort
                Utility.Log("Do not sort", eLogSeverity.information);
                dataObjects = inputPackage.DataObjects.ObjectCollection.DataObject;
            }

            // Iterate through the data objects collection of the package and process each data object individually
            Utility.Log("Iterate through the data objects collection of the package and process each data object individually", eLogSeverity.information);
            for(DataObject dataObject : dataObjects) {
                // Process object    
                invocationResult = dataObject.ProcessDataObject(context, mapping, _ClsLdr);

                // Check result, and update the data object
                if (invocationResult == null) {
                    // Unexpected invocation result
                    throw new Exception("dataObject.ProcessDataObject returned Null as invocation result!");
                }
                
                switch (invocationResult.result) {
                    case Error:
                        Utility.setObjectProcessedError(_ReturnPackage, dataObject, invocationResult.exceptionText, invocationResult.exceptionMessage, invocationResult.ProcessLog, 
                        		inputPackage.PackageId, (inputPackage.DataObjects.ObjectCollection.DataObject.indexOf(dataObject) + 1));
                        break;
                    case Warning:
                        Utility.setObjectProcessedWarning(_ReturnPackage, dataObject, invocationResult.exceptionText, invocationResult.exceptionMessage);
                        break;
                    case Success:
                        Utility.setObjectProcessedSuccesfully(_ReturnPackage, dataObject, invocationResult.ProcessLog);
                        break;
                }    
            }
			
            // Finished
            Utility.Log("Finished", eLogSeverity.information);
	        
	        // Finished, return result as string
			return Response.status(200).entity(_ReturnPackage).build();
		}
		catch (Exception ex) {             
			return Response.status(500).entity(ex).build(); 
		} 		
	}
}